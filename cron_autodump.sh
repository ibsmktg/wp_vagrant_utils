#!/bin/sh
#
# This script runs from cron every N minutes.  The idea is this:
#      1.  If we are working in wordpress and make changes such as new plugins,
#          the wp_options table changes
#			 2.  In order to capture those changes, we want to have them in a dump 
#          file which we track in GIT.
#      3.  There are two possible reasons why the DB dump doesn't match the 
#          file on the filesystem
#            a.  Changes have been made in WP which aren't yet persisted to 
#                the filesystem file
#            b.  The dev did a git pull/git update and pulled in changes from 
#                a different environment but those haven't been loaded yet.
#      4.  In the event of 3a, this script is attempting to auto-dump changes 
#          to the DB so that they can be picked up as needing commit by GIT.  
#          We are attempting to catch the case of 4b and only automatically 
#          put the dump in place if we are overwriting what we last put in 
#          place (i.e. )
#
#  if there is no file there, then create an empty one and we'll see with what is in the DB
#
if [ ! -f /vagrant/wp_options.dump ]; then
	touch /vagrant/wp_options.dump
fi

CURRENT_CHECKSUM=`sum -r /vagrant/wp_options.dump`
if [ -f /vagrant/wp_options.cksum ]; then
	LAST_KNOWN_CHECKSUM=`cat /vagrant/wp_options.cksum`
else
	LAST_KNOWN_CHECKSUM=$CURRENT_CHECKSUM
fi

if [ "$CURRENT_CHECKSUM" = "$LAST_KNOWN_CHECKSUM" ]; then
	# get a dump and compare to current
	/vagrant/wp_vagrant_utils/dump_wp_options.sh > /tmp/wpo.tmp
	CURRENT_CHECKSUM=`sum -r /tmp/wpo.tmp`
	if [ "$LAST_KNOWN_CHECKSUM" = "$CURRENT_CHECKSUM" ]; then
		rm /tmp/wpo.tmp
	else
		mv /tmp/wpo.tmp /vagrant/wp_options.dump; 
	fi
	echo "$CURRENT_CHECKSUM" > /vagrant/wp_options.cksum
else
        # Place a file in the /vagrant directory saying that wp_options 
        # needs to be loaded
   echo " 
        We have detected that the load_wp_options.dump file has been 
        updated from GIT and does not match your database.  To get 
        things back in sync, do the following: 
           $ vagrant ssh
           $ /vagrant/wp_vagrant_utils/load_wp_options.sh
           $ exit \n " > /vagrant/NOTICE..RUN.load_wp_options
fi

