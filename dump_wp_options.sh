#!/bin/sh 

TBL_PREFIX='wpdev'
#MYSQLDUMP='mysqldump --add-drop-table=false --single-transaction=true --skip-dump-date --no-create-info--comments=0'
MYSQLDUMP='mysqldump --single-transaction=true --no-create-info --comments=0'
MYSQLDUMP_OPTIONS="-uwpdev -pwpdev wpdev "

MYSQLDUMP_SED1="
  s/^INSERT INTO \`${TBL_PREFIX}_options\` VALUES /REPLACE INTO \`${TBL_PREFIX}_options\` VALUES\n/;
  s/\),\(/),\n(/g;
  s/^CREATE TABLE/CREATE TABLE IF NOT EXISTS/;
  s/NOT NULL default/NOT NULL DEFAULT/;
  s/NOT NULL auto_increment/NOT NULL AUTO_INCREMENT/;"

MYSQLDUMP_SED2="
  /^\([0-9]+,[0-9]+,'(siteurl|admin_email|home|dashboard_widget_options)',[^)]+\),\$/d"

MYSQLDUMP_WHERE='option_name not like "_transient%" and
           option_name not like "_site_transient%" and
           option_name not like "%_gc_time" and
           option_name not like "supercache_%" and
           option_name not in ("akismet_available_servers",
                               "akismet_connectivity_time",
                               "akismet_spam_count",
                               "ngg_next_update",
                               "openid_associations",
                               "openid_nonces",
                               "recently_activated",
                               "recently_edited",
                               "rewrite_rules",
                               "supercache_stats"
           ) order by option_name'


$MYSQLDUMP $MYSQLDUMP_OPTIONS --where="${MYSQLDUMP_WHERE}" ${TBL_PREFIX}_options \
		| sed -r "$MYSQLDUMP_SED1" | sed -r "$MYSQLDUMP_SED2"
